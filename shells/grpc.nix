# This file is controlled by /etc/dotfiles/README.org
{ pkgs ? import <nixpkgs> { } }:

with pkgs;
mkShell {
  buildInputs = [
    grpc
    grpcui
    grpcurl
    grpc-tools
  ];
  shellHook = ''
  '';
}
